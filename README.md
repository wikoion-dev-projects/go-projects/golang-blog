# Goland-blog
Blog api written in Golang, using Mongodb for datastore, Elasticsearch for searching data and Monstache for Mongo->Elastic sync
## Building
```
git clone https://gitlab.com/wikoion-dev-projects/go-projects/golang-blog.git
cd golang-blog
docker build -t golang-blog:testing .
docker run -d -p 8080:8080 golang-blog:testing
```
## Production deployment
https://gitlab.com/wikoion-dev-projects/deployments/go-reframe-blog-deploy

FROM golang

WORKDIR golang-blog

COPY ./golang-blog ./

RUN openssl genrsa -out https-server.key 2048
RUN openssl ecparam -genkey -name secp384r1 -out https-server.key
RUN openssl req -new -x509 -sha256 -key https-server.key -out https-server.crt -subj "/C=UK/ST=/L=/O=OrgName/OU=IT Department/CN=example.com" -days 3650

RUN go test server/* && go build .

EXPOSE 8080

CMD ["./golang-blog"]
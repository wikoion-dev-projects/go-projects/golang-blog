module gitlab.com/wikoion-dev-projects/go-projects/golang-blog/golang-blog

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/elastic/go-elasticsearch/v7 v7.7.0
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/schema v1.1.0
	github.com/olivere/elastic/v7 v7.0.15
	github.com/rs/zerolog v1.18.0
	go.mongodb.org/mongo-driver v1.3.1
)

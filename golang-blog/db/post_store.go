package db

type PostStore interface {
	GetPost(id string) (Post, error)
	GetPosts(filter *Filter) ([]Post, error)
	CreatePost(post Post) error
	UpdatePost(id string, post Post) error
	DeletePost(id string) error
}

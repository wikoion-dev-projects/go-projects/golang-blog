package db

import (
	"os"
	"context"
	"encoding/json"
	"github.com/rs/zerolog/log"
	elastic "github.com/olivere/elastic/v7"
)


func NewESClient() (*elastic.Client, error) {
	var elasticURL string 

	if os.Getenv("ELASTIC_URL") != "" {
		elasticURL = os.Getenv("ELASTIC_URL")
                log.Info().Msgf("Using provided elastic URL: %s", elasticURL)
	} else {
		elasticURL = "http://elasticsearch:9200"
                log.Info().Msg("Using default elastic URL")
	}

	client, err :=  elastic.NewClient(elastic.SetURL(elasticURL),
		elastic.SetSniff(false),
		elastic.SetHealthcheck(false))

	log.Info().Msg("ES initialized...")

	return client, err
}

func MakeESQuery(query string, esclient *elastic.Client) []Post {
	ctx := context.Background()
	var posts []Post

	searchSource := elastic.NewSearchSource()
	searchSource.Query(elastic.NewMultiMatchQuery(query, "title", "body").Type("phrase_prefix"))

	queryStr, err1 := searchSource.Source()
	queryJs, err2 := json.Marshal(queryStr)

	if err1 != nil || err2 != nil {
		log.Info().Msgf("[esclient][GetResponse]err during query marshal=%s, %s", err1, err2)
		return posts
	}
	log.Info().Msgf("[esclient]Final ESQuery=\n%s", string(queryJs))

	searchService := esclient.Search().Index("coredb.posts").SearchSource(searchSource)
	searchResult, err := searchService.Do(ctx)

	if err != nil {
		log.Error().Err(err).Msg("[ProductsES][GetPIds]")
		return posts
	}

	hits := searchResult.Hits.Hits

	// resultsChannel := make(chan Post)
	// errChannel := make(chan error)

	for _, hit := range hits {
		var post Post
		log.Info().Msgf("hit id: %s", hit.Id)
		err := json.Unmarshal(hit.Source, &post)
		if err != nil {
			log.Error().Err(err).Msg("Failed to unmarshal posts")
			return posts
		}
		post.ID = hit.Id

		posts = append(posts, post)
	}

	// for _, hit := range hits {
	// 	go func(h *elastic.SearchHit) {
	// 		var post Post
	// 		errChannel <- json.Unmarshal(hit.Source, &post)
	// 		post.ID = hit.Id
	// 		resultsChannel <- post
	// 	}(hit)
	// }

	// for i := 0; i < len(hits); i++ {
	// 	err := <-errChannel
	// 	if err != nil {
	// 		log.Error().Err(err).Msg("[Getting Students][Unmarshal]")
	// 		return posts
	// 	}
	// }

	// for i := 0; i < len(hits); i++ {
	// 	post := <-resultsChannel
	// 	posts = append(posts, post)
	// 	log.Info().Msgf("recieved post with title: %s", posts[i].Title)
	// }

	return posts
}

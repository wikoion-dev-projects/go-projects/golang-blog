package db

import (
	"reflect"
	"errors"
	"strings"
	"time"
)

type StubPostStore struct {
	Posts []Post
}

// Returns one post matching supplied id (string), it will return
// an empty post and "Post not found" error if a post is not found
func (s *StubPostStore) GetPost(id string) (Post, error) {
	for _, e := range s.Posts {
		if e.ID == id {
			return e, nil
		}
	}
	return Post{}, errors.New("Post not found")
}

func (s *StubPostStore) GetPosts(filter *Filter) ([]Post, error){
	emptyFilter := new(Filter)
	var posts []Post

	// if there are posts in store
	if len(s.Posts) > 0 {
		// if filter is not empty continue
		if !reflect.DeepEqual(filter, emptyFilter) {
			posts = filterPosts(s.Posts, filter)

			// If filter didn't return any posts, return an error
			if len(posts) == 0 {
				return []Post{}, errors.New("Filter did not match any posts in store")
			}
			return posts, nil
		}
		// No filter supplied, return all posts
		return s.Posts, nil
	}
	// Return empty store and error
	return s.Posts, errors.New("No Posts in store")
}

// Takes a post and stores it
func (s *StubPostStore) CreatePost(post Post) error {
	post.Date = time.Now().Format("20060102150405")
	s.Posts = append(s.Posts, post)
	return nil
}

func (s *StubPostStore) UpdatePost(id string, post Post) error {
	emptyPost := Post{}
	dbPost := Post{}

	for _, e := range s.Posts {
		if e.ID == id {
			dbPost = e
		} else {
		    return errors.New("Post not found")
		}
	}

	if !reflect.DeepEqual(dbPost, emptyPost) {
		updatedPost := UpdatePostHelper(&dbPost, post)
		derr := s.DeletePost(id)

		if derr != nil {
			return derr
		}

		cerr := s.CreatePost(*updatedPost)

		if cerr != nil {
			return cerr
		}
	} else {
		return errors.New("Given empty post object")
	}
	return nil
}

func (s *StubPostStore) DeletePost(id string) error {
	counter := 0
	for idx, e := range s.Posts {
		if e.ID == id {
			s.Posts[idx] = s.Posts[len(s.Posts)-1]
			s.Posts = s.Posts[:len(s.Posts)-1]
			counter++
		}
	}
	if counter == 0 {
		return errors.New("Post not found in store")
	}
	return nil
}

func getPostsSliceHelper(posts []Post, e Post, filterField, postField reflect.Value) []Post {
	if !filterField.IsZero() && !postField.IsZero() {
		if filterField.Kind() == reflect.Slice {
			// if post slice contains all of filter slice continue
			postSlice := postField.Interface().([]string)
			filterSlice := filterField.Interface().([]string)

			if strings.Contains(strings.Join(postSlice, " "), strings.Join(filterSlice, " ")) {
				posts = append(posts, e)
			}
		}
	}
	return posts
}

func getPostsStringHelper(posts []Post, e Post, filterField, postField reflect.Value) []Post {
	if !filterField.IsZero() && !postField.IsZero() {
		if postField.Kind() == reflect.String &&
			filterField.Kind() == reflect.String {
				if postField.String() == filterField.String() {
					posts = append(posts, e)
				}
			}
	}
	return posts
}

func filterPosts(posts []Post, filter *Filter) []Post {
	var filteredPosts []Post

	// for each post (e) do
	for _, e := range posts {
		postElems := reflect.ValueOf(&e).Elem()
		filterElems := reflect.ValueOf(filter).Elem()

		// iterate through all fields supplied in filter
		for i := 0; i < filterElems.NumField(); i++ {
			filterField := filterElems.Field(i)
			postField := postElems.Field(i)

			// If each non zero field value in post matches filter then append to posts
			filteredPosts = getPostsStringHelper(filteredPosts, e, filterField, postField)
			filteredPosts = getPostsSliceHelper(filteredPosts, e, filterField, postField)
		}
	}
	return filteredPosts
}

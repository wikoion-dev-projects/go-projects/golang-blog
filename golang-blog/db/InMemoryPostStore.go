package db

import (
	"gitlab.com/wikoion-dev-projects/go-projects/golang-blog/golang-blog/models"
	"reflect"
	"errors"
)

type Post = models.Post
type Filter = models.Filter

func NewInMemoryPostStore() *InMemoryPostStore {
	return &InMemoryPostStore{
		[]Post {},
	}
}

type InMemoryPostStore struct {
	posts []Post
}

func (i *InMemoryPostStore) GetPost(id string) (Post, error) {
	for _, e := range i.posts {
		if e.ID == id {
			return e, nil
		}
	}
	return Post{}, errors.New("Post not found")
}

func (i *InMemoryPostStore) GetPosts(filter *Filter) ([]Post, error) {
	if len(i.posts) > 0 {
		return i.posts, nil
	}

	return i.posts, errors.New("No Posts in store")
}

func (i *InMemoryPostStore) CreatePost(post Post) error {
	i.posts = append(i.posts, post)
	return nil
}

func (i *InMemoryPostStore) UpdatePost(id string, post Post) error {
	emptyPost := Post{}

	dbPost, err := i.GetPost(id)

	if err != nil {
		return errors.New("Post does not exist")
	}

	if !reflect.DeepEqual(dbPost, emptyPost) {
		updatedPost := UpdatePostHelper(&dbPost, post)
		derr := i.DeletePost(id)

		if derr != nil {
			return derr
		}
		cerr := i.CreatePost(*updatedPost)

		if cerr != nil {
			return cerr
		}
	}
	return errors.New("Given empty post object")
}

func (i *InMemoryPostStore) DeletePost(id string) error {
	counter := 0
	for idx, e := range i.posts {
		if e.ID == id {
			i.posts[idx] = i.posts[len(i.posts)-1]
			i.posts = i.posts[:len(i.posts)]
			counter++
		}
	}
	if counter == 0 {
		return errors.New("Post not found in store")
	}
	return nil
}

package db

import (
	"reflect"
)

func UniqueStringSlice(input []string) []string {
	u := make([]string, 0, len(input))
	m := make(map[string]bool)

	for _, val := range input {
		if _, ok := m[val]; !ok {
			m[val] = true
			u = append(u, val)
		}
	}
	return u
}

func UpdatePostHelper(updated *Post, updater Post) *Post {
	updatedElem := reflect.ValueOf(updated).Elem()
	updaterElem := reflect.ValueOf(updater)

	for i := 0; i < updaterElem.NumField(); i++ {
		updatedField:= updatedElem.Field(i)
		updaterField := updaterElem.Field(i)

		if updaterField.Kind() == reflect.String {
			if updaterField.String() != "" && updatedField.CanAddr() {
				updatedField.SetString(updaterField.String())
			}
		}

		if updaterField.Kind() == reflect.Slice {
			if !updaterField.IsZero(){
				updated.Tags = UniqueStringSlice(append(updated.Tags, updater.Tags...))
			}
		}
	}
	return updated
}

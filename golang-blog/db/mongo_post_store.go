package db

import(
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"github.com/rs/zerolog/log"
	"reflect"
	"errors"
	"fmt"
	"time"
)

func NewMongoPostStore(coreDB *mongo.Database) *MongoPostStore {
	postCollection := coreDB.Collection("posts")
	log.Info().Msg("Successfully created collection posts")

	return &MongoPostStore{
		postCollection,
	}
}

type MongoPostStore struct {
	posts *mongo.Collection
}

func (p *MongoPostStore) GetPost(id string) (Post, error) {
	ctx := context.Background()
	var post Post
	objectID, _ := primitive.ObjectIDFromHex(id)

	log.Info().Msgf("Searching db for post with id: %s", objectID)

	err := p.posts.FindOne(ctx, bson.M{"_id": objectID}).Decode(&post)

	if err == nil {
		log.Info().Msgf("Found post %s", post)
		return post, nil
	}
	return Post{}, err
}

func (p *MongoPostStore) GetPosts(filter *Filter) ([]Post, error) {
	ctx := context.Background()
	var posts []Post
	emptyFilter := new(Filter)

	cur, err := p.posts.Find(ctx, *filter)

	// overwrite filter with empty bson if filter is empty
	if reflect.DeepEqual(emptyFilter, filter) {
		log.Info().Msg("empty filter provided, will return all posts")
		cur, err = p.posts.Find(ctx, bson.M{})
	}

	log.Info().Msgf("Searching with filter: %s", *filter)
	
	if err == nil {
		cur.All(ctx, &posts)
		return posts, nil
	}
	return []Post{}, err
}

func (p *MongoPostStore) CreatePost(post Post) error {
	ctx := context.Background()
	post.Date = time.Now().Format("20060102150405")
	
	inserted, err := p.posts.InsertOne(ctx, post)

	if err != nil {
		log.Error().Err(err).Msgf("Failed to create post %s", inserted)
	} else {
		log.Info().Msgf("Inserted post %s", inserted)
	}
	return err
}

func (p *MongoPostStore) UpdatePost(id string, post Post) error {
	ctx := context.Background()
	emptyPost := Post{}
	
	dbPost, err := p.GetPost(id)

	if err != nil {
		return err
		fmt.Println(err)
	}

	if !reflect.DeepEqual(dbPost, emptyPost) {
		updatedPost := UpdatePostHelper(&dbPost, post)

		derr := p.DeletePost(id)
		if derr != nil {
			return derr
			fmt.Println(derr)
		}

		_, cerr := p.posts.InsertOne(ctx, *updatedPost)
		if cerr != nil {
			return cerr
			fmt.Println(cerr)
		}
	} else {
		fmt.Println("given empty post")
		return errors.New("Given empty post")
	}
	return nil
}

func (p *MongoPostStore) DeletePost(id string) error {
	ctx := context.Background()
	objectID, _ := primitive.ObjectIDFromHex(id)

	log.Info().Msgf("Searching db for post with id: %s", objectID)

	_, err := p.posts.DeleteOne(ctx, bson.M{"_id": objectID})
	if err != nil {
		return err
	}
	return nil
}

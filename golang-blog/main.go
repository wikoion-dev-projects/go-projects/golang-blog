package main

import (
	"net/http"
	"context"
	"time"
	"os"
	"io/ioutil"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"gitlab.com/wikoion-dev-projects/go-projects/golang-blog/golang-blog/db"
	"gitlab.com/wikoion-dev-projects/go-projects/golang-blog/golang-blog/server"
)

var mongoDSN string 

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	ctx, _ := context.WithTimeout(context.Background(), 120*time.Second)
	client := dbInit(ctx)
	log.Info().Msg("Successful DB init")

	coreDB := client.Database("coreDB")
	log.Info().Msg("Successful creation of database")

	store := db.NewMongoPostStore(coreDB)
	log.Info().Msg("Successful creation of mongo post store")

	server := server.NewPostServer(store)

	// if err := http.ListenAndServeTLS(":8443", "https-server.crt", "https-server.key", server); err != nil {
	// 	log.Fatal().Err(err).Msg("could not listen on port 8080")
	// }

	if err := http.ListenAndServe(":8080", server); err != nil {
		log.Fatal().Err(err).Msg("could not listen on port 8080")
	}

	defer client.Disconnect(ctx)
	log.Info().Msg("Connection to DB closed")
}

func dbInit(ctx context.Context) *mongo.Client {
	mongoDefaultURL := "mongodb://madmin:madmin@database:27017/?replicaSet=rs0"
	mongoURL := os.Getenv("MONGO_DSN_FILE")

	if mongoURL != "" {
		content, err := ioutil.ReadFile(mongoURL)
		if err == nil {
			mongoDSN = string(content) 
			log.Info().Msgf("Using provided mongoDSN: %s", mongoDSN)
		} else {
			mongoURL = mongoDefaultURL
			log.Error().Err(err).Msg("Error detected, failing back to default mongo URL")
		}
	} else {
		mongoURL = mongoDefaultURL
                log.Info().Msg("Using default mongo URL")
	}

	client, err := mongo.NewClient(options.Client().ApplyURI(mongoDSN))

	if err != nil {
		log.Fatal().Err(err).Msg("Couldn't create mongo client")
	}

	err = client.Connect(ctx)

	if err != nil {
		log.Fatal().Err(err).Msg("Couldn't connect to client")
	}

	err = client.Ping(ctx, readpref.Primary())

	if err != nil {
		log.Fatal().Err(err).Msg("Client failed to respond to connection ping")
	}

	return client
}

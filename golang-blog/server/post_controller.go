package server

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"github.com/rs/zerolog/log"
	"gitlab.com/wikoion-dev-projects/go-projects/golang-blog/golang-blog/db"
	"gitlab.com/wikoion-dev-projects/go-projects/golang-blog/golang-blog/models"
	"net/http"
	"reflect"
)

type Post = models.Post
type Filter = models.Filter
type PostStore = db.PostStore

type PostServer struct {
	store PostStore
	http.Handler
}

func (p *PostServer) getPostHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	query := vars["id"]

	AssertHTTPMethod(w, r, http.MethodGet)

	post, err := p.store.GetPost(query)

	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		log.Error().Err(err).Msg("Failed to fetch post")
		return
	}

	if post.Author != "" {
		log.Info().Msgf("Successfully fetched post %s", post)
		SetResponseHeaders(w, r)
		json.NewEncoder(w).Encode(post)
	} else {
		w.WriteHeader(http.StatusNotFound)
		log.Info().Msg("Post search didn't return any results")
	}
}

func (p *PostServer) getPostsHandler(w http.ResponseWriter, r *http.Request) {
	esclient, err := db.NewESClient()
	SetResponseHeaders(w, r)

	if err != nil {
		log.Error().Err(err).Msg("Error initializing")
	}

	var posts []Post
	if err := r.ParseForm(); err != nil {
		log.Error().Err(err).Msg("Error parsing form")
	}

	filter := new(Filter)

	if err := schema.NewDecoder().Decode(filter, r.Form); err != nil {
		log.Error().Err(err).Msg("Error decoding filter")
	}

	err = matchAll(filter)
	if err == nil {
		if filter.MatchAll != "" {
			log.Info().Msgf("MatchAll filter supplied %s", filter.MatchAll)
			posts = db.MakeESQuery(filter.MatchAll, esclient)

			if len(posts) > 0 {
				json.NewEncoder(w).Encode(posts)
				return
			}

		} else {
			log.Info().Msg("MatchAll not used")
			posts, err = p.store.GetPosts(filter)

			if err != nil {
				log.Error().Err(err).Msg("Error getting posts from store")
				w.WriteHeader(http.StatusNotFound)
				return
			}

			json.NewEncoder(w).Encode(posts)
			return
		}
	} else {
		log.Info().Msg("MatchAll used incorrectly, with other filters")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	return
}

func (p *PostServer) createPostHandler(w http.ResponseWriter, r *http.Request) {
	if IsAuthorised(w, r) {
		post := new(Post)

		AssertHTTPMethod(w, r, http.MethodPost)
		decodedPost, err := AssertJSONPost(w, r, post)

		if err == nil {
			if AssertHasAllRequiredFields(post) {
				err := p.store.CreatePost(*decodedPost)
				log.Info().Msgf("Decoded post %s", *decodedPost)

				if err != nil {
					w.WriteHeader(http.StatusBadRequest)
					log.Error().Err(err).Msg("Failed to store post")
					return
				}

				w.WriteHeader(http.StatusAccepted)
				log.Info().Msgf("Successfully created post %s", *decodedPost)
			} else {
				w.WriteHeader(http.StatusBadRequest)
				log.Error().Msgf("Post does not contain all required fields %s", post)
			}

			w.WriteHeader(http.StatusAccepted)
			log.Info().Msgf("Successfully created post %s", *decodedPost)
			return
		} else {
			w.WriteHeader(http.StatusBadRequest)
			log.Error().Msgf("Post does not contain all required fields %s", post)
			return
		}
		log.Error().Err(err).Msg("Failed to decode post")
	}
}

func (p *PostServer) updatePostHandler(w http.ResponseWriter, r *http.Request) {
	if IsAuthorised(w, r) {
		post := new(Post)
		vars := mux.Vars(r)
		query := vars["id"]

		AssertHTTPMethod(w, r, http.MethodPut)
		decodedPost, err := AssertJSONPost(w, r, post)

		if err == nil {
			err := p.store.UpdatePost(query, *decodedPost)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				log.Error().Err(err).Msg("Failed to store post")
				return
			}
			w.WriteHeader(http.StatusAccepted)
			log.Info().Msgf("Successfully Updated post %s", *decodedPost)
		}
		w.WriteHeader(http.StatusBadRequest)
		log.Error().Err(err).Msg("Failed to decode post")
	}
}

func (p *PostServer) deletePostHandler(w http.ResponseWriter, r *http.Request) {
	if IsAuthorised(w, r) {
		emptyPost := Post{}

		vars := mux.Vars(r)
		query := vars["id"]

		AssertHTTPMethod(w, r, http.MethodDelete)

		post, err := p.store.GetPost(query)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			log.Error().Err(err).Msg("Failed to fetch post from db")
			return
		}

		if !reflect.DeepEqual(post, emptyPost) {
			err := p.store.DeletePost(query)

			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				log.Error().Err(err).Msg("Failed to delete post in db")
				return
			}
			w.WriteHeader(http.StatusOK)
			log.Info().Msgf("Successfully deleted post %s", post)
		} else {
			w.WriteHeader(http.StatusBadRequest)
			log.Error().Err(err).Msg("post doesn't match post in db")
		}
	}
}

func (p *PostServer) optionsPostHandler(w http.ResponseWriter, r *http.Request) {
	SetResponseHeaders(w, r)
	return
}

func NewPostServer(store PostStore) *PostServer {
	p := new(PostServer)

	p.store = store

	router := mux.NewRouter()

	router.HandleFunc("/posts", p.getPostsHandler).Methods("GET")
	router.HandleFunc("/posts/{id}", p.getPostHandler).Methods("GET")
	router.HandleFunc("/posts", p.createPostHandler).Methods("POST")
	router.HandleFunc("/posts/{id}", p.updatePostHandler).Methods("PUT")
	router.HandleFunc("/posts/{id}", p.deletePostHandler).Methods("DELETE")
	router.HandleFunc("/posts", p.optionsPostHandler).Methods("OPTIONS")

	p.Handler = router
	return p
}

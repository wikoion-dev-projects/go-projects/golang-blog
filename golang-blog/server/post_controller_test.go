package server

import (
	"encoding/json"
	"fmt"
	"gitlab.com/wikoion-dev-projects/go-projects/golang-blog/golang-blog/db"
	"net/http"
	"net/http/httptest"
	"testing"
)

type StubPostStore = db.StubPostStore

func TestGetPost(t *testing.T) {
	store := StubPostStore{
		[]Post{
			{ID: "1", Author: "Johnson", Title: "Test Post 1"},
			{ID: "2", Author: "Dick", Title: "Test Post 2"},
		},
	}

	server := NewPostServer(&store)

	t.Run("returns a blog post json blob", func(t *testing.T) {
		request, _ := GetRequestHelper(t, "/1")
		response := httptest.NewRecorder()

		var got Post

		server.ServeHTTP(response, request)

		err := json.NewDecoder(response.Body).Decode(&got)

		if err != nil {
			t.Fatalf("Unable to parse response from server %q into slice of Player, '%v'", response.Body, err)
		}

		AssertStatus(t, response.Code, http.StatusOK)
		AssertContentType(t, response, "application/json")

		p, err := store.GetPost("1")

		if err != nil {
			t.Errorf("GetPost returned error %v", err)
		}
		AssertEqualPosts(t, got, p)
	})

	t.Run("returns another blog post json blog", func(t *testing.T) {
		request, _ := GetRequestHelper(t, "/2")
		response := httptest.NewRecorder()

		var got Post

		server.ServeHTTP(response, request)

		err := json.NewDecoder(response.Body).Decode(&got)

		if err != nil {
			t.Fatalf("Unable to parse response from server %q into slice of Player, '%v'", response.Body, err)
		}

		AssertStatus(t, response.Code, http.StatusOK)
		AssertContentType(t, response, "application/json")

		p, err := store.GetPost("2")

		if err != nil {
			t.Errorf("GetPost returned error %v", err)
		}

		AssertEqualPosts(t, got, p)
	})

	t.Run("returns 404 on missing post", func(t *testing.T) {
		request, _ := GetRequestHelper(t, "/3")
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		AssertStatus(t, response.Code, http.StatusNotFound)
	})

	t.Run("Returns 404 on incorrect request method", func(t *testing.T) {
		request, _ := http.NewRequest(http.MethodPost, "/post/1", nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		AssertStatus(t, response.Code, http.StatusNotFound)
	})
}

func TestGetPosts(t *testing.T) {
	store := StubPostStore{
		[]Post{
			{ID: "1", Author: "1", Title: "20", Tags: []string{"test1", "test2"}},
			{ID: "2", Author: "2", Title: "10"},
			{ID: "3", Author: "2", Title: "30"},
		},
	}

	server := NewPostServer(&store)

	t.Run("returns a json blob of all posts", func(t *testing.T) {
		request, _ := GetRequestHelper(t, "")
		response := httptest.NewRecorder()

		var got []Post

		server.ServeHTTP(response, request)

		err := json.NewDecoder(response.Body).Decode(&got)

		if err != nil {
			t.Fatalf("Unable to parse response from server %q into slice of Player, '%v'", response.Body, err)
		}

		AssertStatus(t, response.Code, http.StatusOK)
		AssertContentType(t, response, "application/json")

		p, err := store.GetPosts(&Filter{})

		if err != nil {
			t.Errorf("GetPost returned error %v", err)
		}

		AssertEqualStores(t, got, p)
	})

	t.Run("Returns a json blob of all posts matching filter", func(t *testing.T) {
		request, _ := GetRequestHelper(t, "")
		response := httptest.NewRecorder()

		query := request.URL.Query()
		query.Add("Author", "2")
		request.URL.RawQuery = query.Encode()

		var got []Post
		want := []Post{
			{ID: "2", Author: "2", Title: "10"},
			{ID: "3", Author: "2", Title: "30"},
		}

		server.ServeHTTP(response, request)

		err := json.NewDecoder(response.Body).Decode(&got)

		if err != nil {
			t.Fatalf("Unable to parse response from server %q into slice of Player, '%v'", response.Body, err)
		}

		AssertStatus(t, response.Code, http.StatusOK)
		AssertContentType(t, response, "application/json")

		AssertEqualStores(t, got, want)
	})

	t.Run("Returns a json blob of all posts matching filter tags", func(t *testing.T) {
		request, _ := GetRequestHelper(t, "")
		response := httptest.NewRecorder()

		query := request.URL.Query()
		query.Add("Tags", "test1")
		query.Add("Tags", "test2")
		request.URL.RawQuery = query.Encode()

		var got []Post
		want := []Post{
			{ID: "1", Author: "1", Title: "20", Tags: []string{"test1", "test2"}},
		}

		server.ServeHTTP(response, request)

		err := json.NewDecoder(response.Body).Decode(&got)

		if err != nil {
			t.Fatalf("Unable to parse response from server %q into slice of Player, '%v'", response.Body, err)
		}

		AssertStatus(t, response.Code, http.StatusOK)
		AssertContentType(t, response, "application/json")

		AssertEqualStores(t, got, want)
	})

	t.Run("Expects 404 error on non matching filter", func(t *testing.T) {
		request, _ := GetRequestHelper(t, "")
		response := httptest.NewRecorder()

		query := request.URL.Query()
		query.Add("Tags", "test3")
		request.URL.RawQuery = query.Encode()

		server.ServeHTTP(response, request)
		AssertStatus(t, response.Code, http.StatusNotFound)
	})
}

func TestCreatePost(t *testing.T) {
	store := StubPostStore{
		[]Post{},
	}
	server := NewPostServer(&store)

	t.Run("Creates a post object from post request", func(t *testing.T) {
		want := Post{
			Author: "Johnson",
			Title:  "Test Post 1",
			Body:   "Test Post Body",
			Image: "https://gitlab.com/wikoion-dev-projects/draycott-live-blog/-/raw/master/src/main/resources/static/images/home-bg.jpg",
		}

		request, _ := PostRequestHelper(t, "/posts", want)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		AssertStatus(t, response.Code, http.StatusAccepted)

		got, err := store.GetPosts(&Filter{})

		if err != nil {
			t.Errorf("GetPosts returned error %v", err)
		}

		AssertPostStoreNotEmpty(t, got)
		AssertEqualDates(t, got[0], want)
		AssertEqualPosts(t, got[0], want)
	})

	t.Run("expects 405 for malformed request", func(t *testing.T) {
		request, _ := http.NewRequest(http.MethodPost, "/posts/1", nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		AssertStatus(t, response.Code, http.StatusMethodNotAllowed)
	})

	t.Run("expects 400 for incomplete form", func(t *testing.T) {
		want := Post{
			Author: "Johnson",
			Body:   "Test Post Body",
		}

		request, _ := PostRequestHelper(t, "/posts", want)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		AssertStatus(t, response.Code, http.StatusBadRequest)
	})
}

func TestDeletePost(t *testing.T) {
	store := StubPostStore{
		[]Post{
			{ID: "1", Author: "Johnson", Title: "Test Post 1"},
		},
	}
	server := NewPostServer(&store)

	t.Run("Deletes one entry", func(t *testing.T) {
		request, _ := DeleteRequestHelper(t, "1")
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		AssertStatus(t, response.Code, http.StatusOK)

		got, _ := store.GetPosts(&Filter{})

		if len(got) != 0 {
			t.Fatalf("failed to delete post")
		}
	})

	t.Run("Attempts to delete a post that doesn't exist, expects badrequest",
		func(t *testing.T) {
			request, _ := DeleteRequestHelper(t, "Test Post 2")
			response := httptest.NewRecorder()

			server.ServeHTTP(response, request)

			AssertStatus(t, response.Code, http.StatusBadRequest)
		})

	t.Run("Expects 401 error on bad token", func(t *testing.T) {
		request, _ := http.NewRequest(http.MethodDelete, "/posts/1", nil)
		apiKey := "blah"
		bearer := "Bearer " + apiKey
		request.Header.Add("Authorization", bearer)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)
		AssertStatus(t, response.Code, http.StatusUnauthorized)
	})
}

func TestUpdatePost(t *testing.T) {
	store := StubPostStore{
		[]Post{
			{ID: "1", Author: "Johnson", Title: "Test Post 1"},
		},
	}
	server := NewPostServer(&store)

	t.Run("updates one entry", func(t *testing.T) {
		want := Post{
			ID:     "1",
			Author: "Johnson",
			Title:  "Test Post 2",
		}

		request, _ := PutRequestHelper(t, fmt.Sprintf("/posts/%v", "1"), want)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		AssertStatus(t, response.Code, http.StatusAccepted)

		got, err := store.GetPosts(&Filter{})

		AssertGetPosts(t, err)
		AssertPostStoreNotEmpty(t, got)
		AssertEqualPosts(t, got[0], want)

	})

	t.Run("updates one entry's tags", func(t *testing.T) {
		want := Post{
			ID:     "1",
			Author: "Johnson",
			Title:  "Test Post 2",
			Tags:   []string{"Test1", "Test2"},
		}

		request, _ := PutRequestHelper(t, fmt.Sprintf("/posts/%v", "1"), want)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		AssertStatus(t, response.Code, http.StatusAccepted)

		got, err := store.GetPosts(&Filter{})

		if err != nil {
			t.Errorf("GetPosts returned error %v", err)
		}

		AssertPostStoreNotEmpty(t, got)
		AssertEqualPosts(t, got[0], want)
	})
}

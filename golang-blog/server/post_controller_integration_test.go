package server

import (
	"net/http/httptest"
	"testing"
	"net/http"
	"encoding/json"
	"bytes"
	"gitlab.com/wikoion-dev-projects/go-projects/golang-blog/golang-blog/db"
	"reflect"
)

func PostControllerIntegrationTest(t *testing.T) {
	store := db.NewInMemoryPostStore()
	server := NewPostServer(store)
	
	t.Run("Stores some json via POST request then attempts to retrieve it", func (t *testing.T) {
		var got Post
		want := Post{Author: "1", Title: "20"}

		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(want)
		
		postRequest, _ := PostRequestHelper(t, "/new-post/", want)
		postResponse := httptest.NewRecorder()

		getRequest, _ := GetRequestHelper(t, want.Author) 
		getResponse := httptest.NewRecorder()

		server.ServeHTTP(postResponse, postRequest)

		AssertStatus(t, postResponse.Code, http.StatusAccepted)

		server.ServeHTTP(getResponse, getRequest)

		DecodePostJSON(t, getResponse, &got)

		if !reflect.DeepEqual(got, want) {
			t.Errorf("error with json response got %v want %v", got, want)
		}
	})
}

package server

import (
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/rs/zerolog/log"
	"os"
	"io/ioutil"
	"fmt"
	"strings"
)

type Claims struct {
	Username string
	jwt.StandardClaims
}

var jwtSecretKey []byte

// CreateJWT func will used to create the JWT while signing in and signing out
func CreateJWT(username string) (response string, err error) {
	claims := &Claims{
		Username:       username,
		StandardClaims: jwt.StandardClaims{},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	file := os.Getenv("API_KEY_FILE")
	content, err := ioutil.ReadFile(file)

	if err == nil {
		jwtSecretKey = []byte(string(content))
		log.Info().Msg("Using provided API key")
	} else {
		jwtSecretKey = []byte("jwt_secret_key")
		log.Info().Msg("Using default API key")
	}

	tokenString, err := token.SignedString(jwtSecretKey)
	if err == nil {
		return tokenString, nil
	}
	return "", err
}

// VerifyToken func will used to Verify the JWT Token while using APIS
func VerifyToken(tokenString string) (email string, err error) {
	file := os.Getenv("API_KEY_FILE")
	content, err := ioutil.ReadFile(file)

	if err == nil {
		secret := strings.TrimSpace(string(content))
                jwtSecretKey = []byte(secret)
                log.Info().Msg("Using provided API key")
	} else {
		jwtSecretKey = []byte("jwt_secret_key")
		log.Info().Msg("Using default API key")
	}

	token, err := jwt.ParseWithClaims(tokenString, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			log.Error().Msg("Invalid token") 
			return nil, fmt.Errorf("unexpected signing method: %s", "err")
		}
		return jwtSecretKey, nil
	})

	if err != nil {
		log.Error().Err(err)
		return "", nil
	}

	if token != nil {
		if claims, ok := token.Claims.(*Claims); ok && token.Valid {
			return claims.Username, nil
		}
		log.Error().Err(err).Msgf("Failed for some reason: token.Valid: %t", token.Valid)
	}
	
	log.Error().Err(err).Msg("If you see this erorr, but not the token valid error, the token must be nil")
	return "", err
}

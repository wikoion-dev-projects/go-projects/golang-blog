package server

import (
	"net/http/httptest"
	"testing"
	"net/http"
	"encoding/json"
	"bytes"
	"fmt"
	"time"
	"reflect"
)

const apiKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VybmFtZSI6ImFkbWluIn0.X5qKoep6z071prwVB4EKUhGvNac18M9SZvpLKgVQaiY"
const bearer = "Bearer " + apiKey

func PostRequestHelper(t *testing.T, reqUrl string, post Post) (*http.Request, error) {
	t.Helper()
	
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(post)
	
	req, err := http.NewRequest(http.MethodPost, reqUrl, b)
	req.Header.Add("Authorization", bearer)
	return req, err
}

func PutRequestHelper(t *testing.T, reqUrl string, post Post) (*http.Request, error) {
	t.Helper()
	
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(post)
	
	req, err := http.NewRequest(http.MethodPut, reqUrl, b)
	req.Header.Add("Authorization", bearer)
	return req, err
}

func AssertStatus(t *testing.T, got, want int) {
	t.Helper()

	if got != want {
		t.Errorf("Wrong status code got %d want %d", got, want)
	}
}

func GetRequestHelper(t *testing.T, reqUrl string) (*http.Request, error) {
	t.Helper()

	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("/posts%s", reqUrl), nil)
	req.Header.Add("Authorization", bearer)

	return req, err
}

func DeleteRequestHelper(t *testing.T, reqUrl string) (*http.Request, error) {
	t.Helper()

	req, err := http.NewRequest(http.MethodDelete, fmt.Sprintf("/posts/%s", reqUrl), nil)
	req.Header.Add("Authorization", bearer)
	return req, err
}

func SetTestTime(t *testing.T, got, want *Post) {
	t.Helper()
	
	testTime := time.Now().Format("20060102150405") 
	got.Date = testTime
	want.Date = testTime
}

func DecodePostJSON(t *testing.T, response *httptest.ResponseRecorder, post *Post) {
	err := json.NewDecoder(response.Body).Decode(post)

	if err != nil {
		t.Fatal("could not decode json into Post")
	}
}

func AssertContentType(t *testing.T, r *httptest.ResponseRecorder, want string) {
	t.Helper()
	if r.Result().Header.Get("content-type") != want {
		t.Errorf("response did not have content-type of application/json, got %v", r.Result().Header)
	}
}

func AssertResponseBody(t *testing.T, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("did not get correct response got %s want %s", got, want)
	}
}

func AssertGetPosts(t *testing.T, err error) {
	t.Helper()
	if err != nil {
		t.Errorf("GetPosts returned error %v", err)
	}
}

func AssertPostStoreNotEmpty(t *testing.T, slice []Post) {
	t.Helper()
	if len(slice) < 1 {
		t.Fatalf("nothing in store!")
	}
}

func AssertEqualPosts(t *testing.T, got, want Post) {
	t.Helper()
	SetTestTime(t, &got, &want)

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v want %v", got, want)
	}
}

func AssertEqualStores(t *testing.T, got, want []Post) {
	t.Helper()

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v want %v", got, want)
	}
}

func AssertEqualDates(t *testing.T, got, want Post) {
	t.Helper()
	layout := "20060102150405"

	if got.Date != time.Now().Format(layout) {
		t.Errorf("server did not set time!")
	}
}

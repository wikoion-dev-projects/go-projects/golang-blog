package server

import (
	"net/http"
	"encoding/json"
	"reflect"
	"errors"
	"fmt"
	"strings"
	"github.com/rs/zerolog/log"
)

func AssertHTTPMethod(w http.ResponseWriter, r *http.Request, method string) {
	if r.Method != method {
		w.WriteHeader(http.StatusBadRequest)
	}
}

func AssertJSONPost(w http.ResponseWriter, r *http.Request, post *Post) (*Post, error) {
	emptyPost := Post{}

	if r.Body == nil {
		w.WriteHeader(http.StatusBadRequest)
		return &emptyPost, errors.New("Empty request body")
	}

	err := json.NewDecoder(r.Body).Decode(post)

	if err != nil || reflect.DeepEqual(post, emptyPost) {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Println(err)
		return &emptyPost, err
	}

	return post, nil
}

func AssertHasAllRequiredFields(post *Post) bool {
	if post.ID != "" {
		return false
	}

	if post.Author == "" {
		return false
	}

	if post.Title == "" {
		return false
	}

	if post.Body == "" {
		return false
	}

	if post.Image == "" {
		post.Image = "https://gitlab.com/wikoion-dev-projects/draycott-live-blog/-/raw/master/src/main/resources/static/images/home-bg.jpg"
	}

	return true
}

func SetCORS(w http.ResponseWriter, r *http.Request) {
	acceptedOrigins := []string{"https://cmj.fossg.news", "http://localhost:3001", "http://localhost:3000"}
	origin := r.Header.Get("Origin")
	log.Info().Msgf("Request from origin %s", origin)
	for _, acceptedOrigin := range acceptedOrigins {
		if acceptedOrigin == origin {
			w.Header().Set("Access-Control-Allow-Origin", acceptedOrigin)
			log.Info().Msgf("Set CORS header for origin %s", acceptedOrigin)
		}
	}
}

func SetResponseHeaders(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	SetCORS(w, r)
} 

func matchAll(filter *Filter) error {
	filterElems := reflect.ValueOf(filter).Elem()

	if filter.MatchAll != "" {
		// iterate through all fields supplied in filter
		for i := 0; i < filterElems.NumField(); i++ {
			filterField := filterElems.Field(i)

			if !filterField.IsZero() {
				errors.New("Error can't have match all and other filters in query")
			} 
		}
		return nil
	}
	return nil
}

func IsAuthorised(w http.ResponseWriter, r *http.Request) bool {
	bearerToken := r.Header.Get("Authorization")
	var bearerSplit []string
	var authorizationToken string

	// Two checks because strings.Split panics if it can't split :shrug:
	if strings.Contains(bearerToken, " ") {
		bearerSplit = strings.Split(bearerToken, " ")
	} else {
		w.WriteHeader(http.StatusUnauthorized)
		log.Error().Msg("Error decoding authorization token")
		return false
	}

	if len(bearerSplit) > 0 {
		authorizationToken = bearerSplit[1]
		log.Info().Msg("Successfully retrieved token...")
	} else {
		w.WriteHeader(http.StatusUnauthorized)
		log.Error().Msg("Error decoding authorization token")
		return false
	}
 
	username, _ := VerifyToken(authorizationToken)

	SetResponseHeaders(w, r)

	if username == "" {
		w.WriteHeader(http.StatusUnauthorized)
		log.Info().Msgf("Unauthorized request with token %s", authorizationToken)
		return false
	}

	if username == "admin" {
		log.Info().Msg("Authorized request for posts")
		return true
	}
	
	w.WriteHeader(http.StatusUnauthorized)
	log.Info().Msgf("Unauthorized request with username %s token %s",username, authorizationToken)
	return false
}

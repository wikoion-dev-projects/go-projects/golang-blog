package models

import (
	"time"
)

type Filter struct {
	ID string`bson:"_id,omitempty" schema:"id,omitempty"`
	Author string `bson:"author,omitempty" schema:"author,omitempty"`
	Title string `bson:"title,omitempty" schema:"title,omitempty"`
	Body string `bson:"body,omitempty" schema:"body,omitempty"`
	Date *time.Time `bson:"date,omitempty" schema:"date,omitempty"`
	Tags []string `bson:"tags,omitempty" schema:"tags,omitempty"`
	MatchAll string `bson:"matchall,omitempty" schema:"matchall,omitempty"`
}

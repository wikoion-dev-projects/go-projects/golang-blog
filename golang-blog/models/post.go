package models

type Post struct {
	ID string `bson:"_id,omitempty"`
	Author string `bson:"author"`
	Title string `bson:"title"`
	Body string `bson:"body"`
	Date string `bson:"date"`
	Tags []string `bson:"tags"`
	Image string `bson:"image"`
}
